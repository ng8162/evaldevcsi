<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: 'Lato', sans-serif;
        }

        p#help {
            font-size: 0.8em
        }
    </style>

</head>

<body>

    <h1>Vérification de code postal</h1>

    <!-- Consigne : Mettre en place un gestionnaire d’événement qui vérifie que 
le code postal renseigné par l’utilisateur est composé de 5 chiffres 
avant la soumission du formulaire.
Documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Expressions_r%C3%A9guli%C3%A8res 
Si le code postal est erroné, le formulaire ne doit pas être envoyé. 
-->

    <form id="form" method="post" action="#">

        <input type="number" name="code_postal" id="code_postal">
        <p id="help">Le code postal doit contenir 5 chiffres</p>

        <input type="submit" value="choisir">

    </form>

    <script>

    form = document.getElementById('form')

    form.addEventListener('submit',function(event){
       let codePostal = document.getElementById("code_postal")

        if (codePostal.value.length !== 5) {
            event.preventDefault()
            let help = document.getElementById('help')
            help.innerHTML = 'ATTENTION le code Postal ne comporte pas 5 chiffres!!!'
        }else{
            let help = document.getElementById('help')
            help.innerHTML = 'le code Postal comporte bien 5 chiffres!!!'
        }
    
    })
    


    </script>

</body>

</html>