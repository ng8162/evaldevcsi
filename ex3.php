<html>

<head>

<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet"> 

    <style>

        body{
            font-family: 'Lato', sans-serif;
        }


        ul {
            width: 60%;
            margin: 0 auto;
            /*Supprimer les puces devant les listes ul*/
            list-style: none;
        }

        ul.hide{
            display:none;
        }

        li{
            /*Donner 0.5 de margin et de padding aux éléments de liste li*/
            margin: 0.5;
            padding : 0.5;
        }

        a{
            font-weight: bold;
            text-decoration: none;
            color:#2c4254;
        }

        a:hover{
            color:#0c141a;
        }

        .niveau-1 >li{
            color:#2c4254;
            
        }
        .niveau-1 > li:nth-child(even) {background: #bbdefb}
        .niveau-1 > :nth-child(odd) {background: #eeffff}

        .niveau-2 >li{
            color:#2c4254;
        }

        /* Consignes
        
            CSS :
            - Supprimer les puces devant les listes ul
            - Donner 0.5 de margin et de padding aux éléments de liste li

            - Graisser les balises <a>; supprimer le soulignement et les colorer en  #2c4254
            - Au survol des balises <a>, les colorer en #0c141a

            - Colorer les éléments <li> contenus dans les <ul> de niveau 1 en #2c4254
            - Colorer les éléments <li> contenus dans les <ul> de niveau 2 en #c48b9f

            - Donner une couleur de fond aux éléments <li> contenus dans les <ul> de niveau 1 selon la règle suivante :
                couleur #bbdefb pourles éléments paires; couleur #eeffff pour les éléments impaires   
        
         */

    </style>

</head>

<body>

<h1>Menu Accordéon</h1>

    <ul class="niveau-1">
        <li>
            <a href="#" class="trigger">Scanner</a>
            <ul class="niveau-2 hide">
                <li>Scanner numériques</li>
                <li>Accessoire</li>
            </ul>
        </li>
        <li>
            <a href="#" class="trigger">Ordinateur</a>
            <ul class="niveau-2 hide">
                <li>Portables</li>
                <li>Tours</li>
            </ul>
        </li>
        <li>
            <a href="#" class="trigger">Reseau</a>
            <ul class="niveau-2 hide">
                <li>Stockage reseau</li>
                <li>Switch</li>
                <li>LAN</li>
            </ul>
        </li>
        <li>
            <a href="#" class="trigger">Cable et connectique</a>
            <ul class="niveau-2 hide">
                <li>Cable reseau</li>
                <li>Cable audio & video</li>
            </ul>
        </li>
        <li>
            <a href="#" class="trigger">Périphérique</a>
            <ul class="niveau-2 hide">
                <li>Haut-parleur</li>
                <li>Souris</li>
                <li>Webcam</li>
                <li>Clavier</li>
            </ul>
        </li>
        <li>
            <a href="#" class="trigger">Stockage</a>
            <ul class="niveau-2 hide">
                <li>clef usb</li>
                <li>disque dur externe</li>
                <li>carte flash</li>
            </ul>
        </li>
    </ul>

    <script>

        // Consignes

        // Objectif : Au click sur une balise <a>, déclencher l'affichage du menu de niveau 2.
        
        // Pour cela, vous devrez :

        // 1. récupérer les éléments de la classe trigger
        var triggers = document.getElementsByClassName('trigger')
        console.log(triggers)
        // 2. pour chacun : ecrire un gestionnaire d'événement
        var trigger
        var liParent

        for (let index = 0; index < triggers.length; index++) {
            const trigger = triggers[index];
            trigger.addEventListener('click',function(){
                // - récupérer le <li> parent au moyen de la propriété "parentNode"
                liParent = trigger.parentNode
                console.log(liParent)
                // - trouver le <ul> de niveau 2 au moyen de la methode querySelector
                var ulNiveau2 = liParent.querySelector('.niveau-2')
                console.log(ulNiveau2)
                // - si le <ul> a la classe "hide" retirez-lui, sinon ajoutez-lui.
                var classUlNiveau2 = ulNiveau2.className
                const searchTerm = 'hide';
                const indexOfFirst = classUlNiveau2.indexOf(searchTerm);
                if (indexOfFirst === -1) {
                    ulNiveau2.classList.add('hide')
                }else{
                    ulNiveau2.classList.remove('hide')
                }
            
            })
        }
        // 3. Dans le gestionnaire d'événment, 
            // - récupérer le <li> parent au moyen de la propriété "parentNode"
            // - trouver le <ul> de niveau 2 au moyen de la methode querySelector
            // - si le <ul> a la classe "hide" retirez-lui, sinon ajoutez-lui.
            // Vous pouvez vous appuyer sur la documentation de MDN : https://developer.mozilla.org/fr/docs/Web/API/Element/classList

        </script>

    </body>

</html>