<html>

<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

<style>
    body {
        font-family: 'Lato', sans-serif;
    }
</style>

<body>


    <h1>Selectionner un langage de programmation</h1>

    <!-- Consigne : Ce formulaire permet à l'utilisateur de choisir un langage de programmation. 
Vous devez ecrire le code php permettant d'afficher à l'écran le langage choisi par l'utilisateur. -->

    <form method="post" action="#">

        <select name="language">

            <option value="0">PHP</option>
            <option value="1">JS</option>
            <option value="2">Python</option>

        </select>

        <input type="submit" value="choisir">

    </form>

    <?php
        //var_dump($_POST);
        if (isset($_POST['language'])) {
            echo 'Vous avez choisi => ';
            if ($_POST['language'] === '0') {
                echo 'PHP';
            }elseif ($_POST['language'] === '1') {
                echo 'JS';
            }elseif ($_POST['language'] === '2') {
                echo 'Python';
            }
        }
    
    ?>

</body>

</html>