<html>

<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

<style>
    body {
        font-family: 'Lato', sans-serif;
    }
</style>


<body>

    <h1>Trouver le jour de la semaine</h1>

    <!-- Consigne : Ce formulaire permet à l'utilisateur de saisir une date au format dd/mm/yyyy. 
Vous devez ecrire le code php permettant d'afficher à l'écran le jour de la semaine correspondant à la date saisie.
Par exemple : si l'utilisateur saisi 16/01/2020, le programma affiche "Jeudi" -->

    <form method="post" action="#">>

        <input type='text' name='date' placeholder="dd/mm/yyyy"></input>

        <input type="submit" value="choisir">

    </form>

    <?php
        
        if (isset($_POST['date'])) {
            $date = str_replace('/','-',$_POST['date']);
            $date1 = date($date);
            setlocale(LC_TIME, "fr_FR", "French");
            echo strftime("%A", strtotime($date1));
        }
    
    ?>

</body>

</html>